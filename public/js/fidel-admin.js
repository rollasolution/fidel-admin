function authenticate(){
	
	jQuery.ajax({
        type: "POST",
        async: true,
        url: 'http://localhost:9090/fidel-core/rest/auth/login',
        dataType: "json",
        headers: {
	        username: $("input[name='username']").val(), 
	        password: $("input[name='password']").val()
	    },
        contentType: "application/json; charset=utf-8",
        success: function (data){ 
        	console.log(data);
        	sessionStorage.setItem("userData", JSON.stringify(data));
        	$.post( "/authenticate", { 'data': data.perfil }, function( data ) {
        		  $( "#content" ).html( data );
        	} );
        },
        error: function (err){ 
        	console.log(err.responseText);
        	$("#errorMessage").toggle();
        }
    });
}



/*
function authenticate(){
	
	jQuery.ajax({
        type: "POST",
        async: true,
        url: '/authenticate',
        beforeSend: function(xhrObj)
        {
            xhrObj.setRequestHeader("Content-Type","application/json");
            xhrObj.setRequestHeader("Accept","application/json");
        },
        headers: {
        	username: $("input[name='email']").val(), 
	        password: $("input[name='password']").val()
	    },
        success: function (data){ 
        	console.log(data);
        	$( "#page-wrapper" ).html( data );
        },
        error: function (err){ 
        	console.log(err.responseText);
        	$("#errorMessage").toggle();
        }
    });
	
}
*/
