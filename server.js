//Modules setup
var express  = require('express');
var multer  = require('multer')

var app      = express();
app.use(multer({ dest: './public/uploads/'}))

var port     = process.env.PORT || 8080;
var passport = require('passport');
var flash 	 = require('connect-flash');
var needle 	 = require('needle');
var morgan       = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser   = require('body-parser');
var session      = require('express-session');

//Application setup 
app.use(morgan('dev')); // log every request to the console
app.use(cookieParser()); // read cookies (needed for auth)

app.use(bodyParser()); // get information from html forms

app.use(bodyParser.urlencoded({
	extended: true
}));
app.use(bodyParser.json())

app.use(express.static(__dirname + '/public'));

app.set('view engine', 'ejs'); // set up ejs for templating

// required for passport
app.use(session({ secret: 'fideladminrules' })); // session secret
app.use(passport.initialize());
app.use(passport.session()); // persistent login sessions
app.use(flash()); // use connect-flash for flash messages stored in session

// passport ======================================================================
require('./config/passport')(passport); // pass passport for configuration

app.set('view engine', 'ejs'); // set up ejs for templating

// routes ======================================================================
require('./app/routes.js')(app, passport); // load our routes and pass in our app and fully configured passport

// launch ======================================================================
app.listen(port);
console.log('The magic happens on port ' + port);
