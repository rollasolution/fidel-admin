// app/services.js
var needle 	 = require('needle');
var context = 'http://localhost:9090/fidel-core/rest';

exports.authenticate = function(username, password, callback){
	
	//Call to authentication web service
	var options = {
	 headers: {
	        'username': username,
	        'password': password
	    }
	}
	
	needle.post(context + '/auth/login', '', options, function(err, resp) {
		if (err || resp.statusCode != 202){
			console.log('Error statusCode : ' + resp.statusCode);
			callback();
		}else{
			callback(resp.body.token);
		}
	});
	
};

exports.usuariosList = function(userToken, callback){

	//Invocar servicio para listar usuarios
	var options = {
		  headers: {  'X-Auth-Token': userToken }
	}
	
	console.log('usuariosList token: ' + userToken)
	
	var url = context + '/usuarios';
	
	needle.get(url, options, function(err, resp, body) {
		if (err || resp.statusCode != 200){
			/*console.log('statusCode: ' + resp.statusCode);
			console.log(JSON.stringify(resp.body));*/
			callback(resp.body);
		}else{
			console.log('Se recupero listado: ' + JSON.stringify(resp.body));
			callback(resp.body);
		}
	});
	
};

exports.perfilesList = function(userToken, callback){

	//Invocar servicio para listar perfiles
	var options = {
		  headers: {  'X-Auth-Token': userToken }
	}
	
	var url = context + '/perfiles';
	
	needle.get(url, options, function(err, resp, body) {
		if (err || resp.statusCode != 200){
			callback(resp.body);
		}else{
			console.log('Se recupero perfiles: ' + JSON.stringify(resp.body));
			callback(resp.body);
		}
	});
	
};

exports.createUser = function(userToken, empresa, req, callback) {

	var user = {"userName":req.body.userName, "password":req.body.password, "nombre":req.body.nombre, "apellido":req.body.apellido, 
	"perfil": "USR", "email": req.body.email};

	var userEmpresa = {"usuario": user, "idEmpresa": empresa.id}
	

	console.log('userEmpresaDTO:', userEmpresa);

	var options = {
	  	headers: {  'X-Auth-Token': userToken, 
	  		'Content-Type': 'application/json'
	  	} 
	}		
	
	var url = context + '/registration/signup';

	needle.put(url, JSON.stringify(userEmpresa), options, function(err, resp) {
		if (err || resp.statusCode != 200){
			console.log('Error en register user');
		}else{
			console.log('0k en register user');
		}
		callback(resp);
	});
};

exports.findUsuarioForSettings = function(req, callback){
	
	var url = context + '/usuarios/settings/' + req.user.userName;

	var options = {
	  	headers: {  
	  		'X-Auth-Token': req.user.token, 
	  		'Content-Type': 'application/json'
	  	} 
	}
	
	needle.get(url, options, function(err, resp, body) {
		if (err || resp.statusCode != 200){
			console.log('Error en find UsuarioForSettings');
			console.log(resp.body);
			callback();
		}else{
			console.log('0k en find UsuarioForSettings');
			console.log(JSON.stringify(resp.body));
			callback(resp.body);
		}
	});
	
};

exports.updateUser = function(req, callback){
	
	var user = {"id":req.body.id, "username":req.body.userName, "password":req.body.password, "email":req.body.email, 
			"nombre":req.body.nombre, "apellido":req.body.apellido};
	
	var url = context + '/usuarios/save';

	var options = {
	  	headers: {  
	  		'X-Auth-Token': req.user.token, 
	  		'Content-Type': 'application/json'
	  	} 
	}	
	
	needle.put(url, JSON.stringify(user), options, function(err, resp, body) {
		if (err || resp.statusCode != 202){
			console.log('Error en save user');
			console.log(resp.body);
			callback(resp.statusCode);
		}else{
			console.log('0k en register user');
			callback(resp.statusCode);
		}
	});
	
}

exports.registerUser = function(req, callback){
	
	var user ={"usuario": 
				{"userName":req.body.userName, "password":req.body.password, "email":req.body.email,
					"nombre":req.body.nombre,"apellido":req.body.apellido, "perfil":"ADMIN"}
				};



	var url = context + '/registration/signup';

	var options = {
	  	headers: {  
	  		'Content-Type': 'application/json'
	  	} 
	}	
	
	console.log('user a enviar : ' + JSON.stringify(user));
	
	needle.put(url, JSON.stringify(user), options, function(err, resp, body) {
		if (err || resp.statusCode != 200){
			console.log('Error en register user');
			console.log(resp.body);
		}else{
			console.log('0k en register user');
		}
		callback(resp);
	});
	
};
	
exports.findEmpresaByCreador = function(req, callback){
	
	//Invocar servicio para listar perfiles
	var options = {
		  headers: {  'X-Auth-Token': req.user.token }
	}
	
	var url = context + '/empresas/usuarios/' + req.user.id;
	
	needle.get(url, options, function(err, resp, body) {
		if (err || resp.statusCode != 200){
			callback(resp.body);
		}else{
			console.log('Se recupero empresa: ' + JSON.stringify(resp.body));
			callback(resp.body);
		}
	});
	
};

exports.registerEmpresa = function(req, callback){
	
	console.log("Usuario para registrar empresa: " + JSON.stringify(req.user));
	console.log("Usuario token para registrar empresa: " + req.user.token);
	
	//Invocar servicio para registrar una empresa
	var options = {
		  headers: {  
			  'X-Auth-Token': req.user.token,
			  'Content-Type': 'application/json'
		  }
	}
	
	var empresa = {"cuit":req.body.cuit, "razonSocial":req.body.razonSocial};
	var idUsuario = req.user.id;
	
	var data = {"idUsuario":idUsuario, "empresa":empresa};
	
	var url = context + '/empresas/save';
	
	console.log('empresa:' + JSON.stringify(empresa));
	
	console.log('URL: ' + url);
	
	needle.put(url, JSON.stringify(data), options, function(err, resp, body) {
		if (err || resp.statusCode != 202){
			console.log('Error: ' + resp.statusCode);
			callback(resp.body);
		}else{
			callback(resp.statusCode);
		}
	});
	
}

exports.activateUser = function(req, callback) {

	var url = context + '/usuarios/activate/' + req.query.userName + '?codigoVerificacion=' + req.query.codigoVerificacion

	console.log(url);

	needle.get(url, function(err, resp){
			callback(resp);
		});

}


exports.registerNovedad = function(req, img, callback) {
 
	
	console.log("Usuario para registrar novedad: " + JSON.stringify(req.user));
	console.log("Usuario token para registrar novedad: " + req.user.token);
	console.log("fecha expira:" +  new Date(req.body.fechaExpira));
	//Invocar servicio para registrar una novedad
	var options = {
		  headers: {  
			  'X-Auth-Token': req.user.token,
			  'Content-Type': 'application/json'
		  }
	}
	 
	var novedad = {"titulo":req.body.titulo, "detalle":req.body.detalle,  "urlImagen": img, 
			"fechaExpira": new Date(req.body.fechaExpira), "idEmpresa" : req.body.idEmpresa};
	var idUsuario = req.user.id;
	
	
	
	var data = {"idUsuario":idUsuario, "novedad":novedad};
	
	var url = context + '/novedades/save';
	
	console.log('novedad:' + JSON.stringify(novedad));
	
	console.log('URL: ' + url);
	
	needle.put(url, JSON.stringify(data), options, function(err, resp, body) {
		if (err || resp.statusCode != 202){
			console.log('Error: ' + resp.statusCode);
			callback(resp.body);
		}else{
			callback(resp.statusCode);
		}
	});
	
};

exports.novedadesList = function(userToken, callback){

	//Invocar servicio para listar novedades
	var options = {
		  headers: {  'X-Auth-Token': userToken }
	}
	
	console.log('novedadesList token: ' + userToken)
	
	var url = context + '/novedades';
	
	needle.get(url, options, function(err, resp, body) {
		if (err || resp.statusCode != 200){
			/*console.log('statusCode: ' + resp.statusCode);
			console.log(JSON.stringify(resp.body));*/
			callback(resp.body);
		}else{
			console.log('Se recupero listado: ' + JSON.stringify(resp.body));
			callback(resp.body);
		}
	});
	
};

