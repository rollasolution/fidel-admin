var mysql 	 = require('mysql');

var connection = mysql.createConnection(
    {
      host     : 'localhost',
      port	   : '3306',		
      user     : 'root',
      password : '',
      database : 'fidel'
    }
);

exports.findUsuario = function(username, callback){

	console.log('findUsuario....');
	
	var queryString = 'SELECT ?? FROM Usuario where userName = ?';
	var columns = ['id', 'apellido', 'nombre', 'email', 'codigoVerificacion', 'perfil',
	               'status', 'userName', 'empresa_id']; 
	
	connection.query(queryString, [columns, username], function(err, result) {
	    if (err) throw err;
	    
	    callback(result);
	});
	 
};


	

	
