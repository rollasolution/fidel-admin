// app/routes.js

// services ======================================================================
var services = require('./services'); // 

//re-captcha ======================================================================
var recaptcha = require('../config/recaptcha');

// Fyle System ======================================================================
var fs = require('fs');

// Graphics Magik ======================================================================
var gm = require('gm').subClass({ imageMagick: true });

module.exports = function(app, passport) {

	// =====================================
	// HOME PAGE (with login links) ========
	// =====================================
	app.get('/', function(req, res) {
		res.render('index.ejs'); // load the index.ejs file
	});

	// =====================================
	// LOGIN ===============================
	// =====================================
	// show the login form
	app.get('/login', function(req, res) {
		res.setHeader(
        "Cache-Control",
        "no-cache, max-age=0, must-revalidate, no-store");
		
		// render the page and pass in any flash data if it exists
		res.render('login.ejs', { message: req.flash('loginMessage') });
		//res.render('login.ejs');
	});

	// process the login form
	app.post('/login', passport.authenticate('local-login', {
		successRedirect : '/profile', // redirect to the secure profile section
		failureRedirect : '/login', // redirect back to the signup page if there is an error
		failureFlash : true // allow flash messages
	}));

	// =====================================
	// SIGNUP ==============================
	// =====================================
	// show the signup form
	app.get('/signup', function(req, res) {
		recaptcha.renderChallenge( function(result){
			console.log('callback trae : ' + JSON.stringify(result));
			
			// render the page and pass in any flash data if it exists
			res.render('signup.ejs', { 
				recaptcha_form: result
			});
		});
	});

	// process the signup form
	app.post('/signup', function(req, res) {
		
		recaptcha.verifyChallenge( req, function(result){
			
			if (result != '0k'){
				// render the page and pass in any flash data if it exists
				res.render('signup.ejs', { 
					message: 'Captcha verification is invalid.',
					recaptcha_form: result
				});
			}else{
				console.log('todo 0k...email : ' + req.body.email);
				
				services.registerUser(req, function(registerResult){
					
					if(registerResult.statusCode != 200){
						//Pido un nuevo challenge
						recaptcha.renderChallenge( function(resultCaptcha){
							res.render('signup.ejs', { 
								messageWarning: warningMessage(registerResult.body),
								recaptcha_form: resultCaptcha
							});
						});	
						
					}else{
						res.render('signupok.ejs', { 
							email: req.body.email
						});
					}
				});
			}
		});

	});
	
	function warningMessage(message) {
		switch(message) {
	    case "USER_EXISTS":
	        return 'Ya existe un usuario con ese nombre.';
	    case "EMAIL_EXISTS":
	    	return 'Ya existe una cuenta asociada a ese e-mail.';
	    default:
	        return 'Alta de usuario inválida';
		}
	}
	
	
	// =====================================
	// PROFILE SECTION =========================
	// =====================================
	// we will want this protected so you have to be logged in to visit
	// we will use route middleware to verify this (the isLoggedIn function)
	app.get('/profile', isLoggedIn, function(req, res) {
		
		console.log('* * * * Usuario logeado: ' + JSON.stringify(req.user));

		//Checkeo si el usuario ya tiene una empresa registrada.  Sino, fwd a "registre su empresa"
		services.findEmpresaByCreador(req, function(result) {
			console.log("findEmpresaByCreador result:" + result);
			console.log(req.user);
			if (result || req.user.perfil == 'user'){
				res.render('dashboard.ejs', {
					user : req.user // get the user out of session and pass to template
				});
			}else{
				res.redirect('/empresa-create');
			}
		});
		
	});

	

	// =====================================
	// SUCURSALES SECTION =========================
	// =====================================
	
	app.get('/sucursales', isLoggedIn, function(req, res) {
		res.render('sucursales.ejs', {
			user : req.user // get the user out of session and pass to template
		});
	});
	
	
	// =====================================
	// USUARIOS SECTION =========================
	// =====================================
	
	app.get('/usuarios', isLoggedIn, function(req, res) {
		
		services.usuariosList(req.user.token, function(result){
			console.log('callback trae : ' + JSON.stringify(result));
			
			res.render('usuario-admin.ejs', {
				user : req.user, // get the user out of session and pass to template
				userList : result // listado de usuarios
			});
		});
	});
	
	app.get('/usuario-create', isLoggedIn, function(req, res) {
		
		services.perfilesList(req.user.token, function(result) {
			console.log('callback trae : ' + JSON.stringify(result));
			
			res.render('usuario-create.ejs', {
				user : req.user//, // get the user out of session and pass to template
				//profiles : result 
			});
		});
	});
	
	app.post('/usuario-create', isLoggedIn, function(req, res) {


		services.findEmpresaByCreador(req, function(empresa) {
		
			services.createUser(req.user.token, empresa, req, function(callback) {

				console.log('A ver como llega la empresa:', empresa);

				if(callback.statusCode != 200) {

					console.log('paso por el error');
					res.render('usuario-create.ejs', {
						user : req.user,
						message: warningMessage(callback.body)
					});
				} else {
					res.redirect('/usuario-create');
				}
			});	
		});
	});
	
	app.get('/activate', function(req, res) {
		services.activateUser(req, function(callback) {
			if(callback.statusCode != 200) {
					res.render('index.ejs', {
					message: 'Invalid verification'});
			} else {
				res.render('login.ejs', {
					messageSuccess: 'Por favor, ingrese con su usuario y password.'
						});
			}
		});
	});


	// =====================================
	// EMPRESA SECTION =========================
	// =====================================
	
	app.get('/empresa-create', isLoggedIn, function(req, res) {
		recaptcha.renderChallenge( function(result){
			console.log('callback trae : ' + JSON.stringify(result));
			
			res.render('empresa.ejs', { 
				user : req.user,
				recaptcha_form: result
			});
		});
	});
	
	app.post('/empresa-create', isLoggedIn, function(req, res) {

		recaptcha.verifyChallenge( req, function(result){
			
			console.log('Verify challenge.....');
			
			if (result == '0k'){
				services.registerEmpresa(req, function(result){
			
					if (result == 202){
						res.redirect('/profile');
					}else{
						recaptcha.renderChallenge( function(resultCaptcha){
							res.render('empresa.ejs', { 
								user : req.user,
								recaptcha_form: resultCaptcha,
								message: 'Ya existe una empresa registrada con ese CUIT/Razón Social'
							});
						});	
					}
				});
			} else {
				res.render('empresa.ejs', { 
					message: 'Captcha verification is invalid.',
					recaptcha_form: result
				});
			}	
	});

});
	
	
	// =====================================
	// NOVEDADES SECTION =========================
	// =====================================
	
	app.get('/novedades', isLoggedIn, function(req, res) {
		
		services.novedadesList(req.user.token, function(result){
			console.log('callback trae : ' + JSON.stringify(result));
			
			res.render('novedad-admin.ejs', {
				user : req.user, // get the user out of session and pass to template
				novedadesList : result // listado de novedades
			});
		});
	});
		
	app.get('/novedad-create', isLoggedIn, function(req, res) {
						
			res.render('novedad-create.ejs', {
				user : req.user//, // get the user out of session and pass to template 	
		});
	});
		
	app.post('/novedad-create', isLoggedIn, function(req, res) {

        var  dir = __dirname + '/../public/';
        //Path default donde las imagenes son guardadas
        //console.log('Path: ' + req.files.image_file.path);

        var origFile = req.files.image_file.path;
        var outFile = 'uploads/cropped/' + req.files.image_file.name;
        
        
        console.log('Original Filename: ' + origFile);
        console.log('Out Filename: ' + outFile);
        
        console.log('dir + outfile : ' + dir + outFile);
//
//        console.log('X1: ' + req.body.x1);
//        console.log('Y1: ' + req.body.y1);
//        console.log('W: ' + req.body.w);
//        console.log('H: ' + req.body.h);
        
        if (req.body.x1 !== undefined ){
        	   // your code here.
		        gm(origFile)
		            .crop(req.body.w, req.body.h, req.body.x1, req.body.y1)
		            .write(dir + outFile, function (err) {
		                    if (!err) console.log('crazytown has arrived');
		                    else console.log('error: ' + err);
		                    fs.unlink(origFile, function(err) {
		                        if (err) console.log('error deleting uploaded file: ' + err);
		                        //  res.send('File uploaded to: ' + target_path + ' - ' + req.files.thumbnail.size + ' bytes');
		                    });
		                });
        }else{
        	 gm(origFile).write(dir + outFile, function (err) {
                     if (!err) console.log('La imagen no fue croppedada, se guarda como se subio');
                     else console.log('error: ' + err);
                     fs.unlink(origFile, function(err) {
                         if (err) console.log('error deleting uploaded file: ' + err);
                     });
                 });       	
        };
        //TODO: Establecer directorio por promocion?

			services.registerNovedad(req, outFile, function(callback) {

				if(callback != 202) {

					console.log('paso por el error');
					res.render('novedad-create.ejs', {
						user : req.user,
						message: 'Ha ocurrido un error ' + callback
					});
				} else {
					res.render('novedad-create.ejs', {
						user : req.user,
						message: 'Novedad creada con exito '
					});
				}
			});	
	});
	
	
	
	
	// =====================================
	// SETTINGS SECTION =========================
	// =====================================
	app.get('/settings', isLoggedIn, function(req, res) {
		
		//Recupero usuarioDTO para mostrar datos en preferencias
		services.findUsuarioForSettings(req, function(data){
			
			if (data){
				res.render('settings.ejs', {
					user : data // get the user out of session and pass to template
				});
			}else{
				res.render('settings.ejs', {
					user : req.user, // get the user out of session and pass to template
					message: 'Error en settings'
				});
			}
		});
		
	});
	
	app.post('/settings', isLoggedIn, function(req, res) {
		
		services.updateUser(req, function(data){
			
			if (data==202){
				res.render('settings.ejs', {
					user : req.user, // get the user out of session and pass to template
					message: 'Sus datos fueron actualizados.'
				});
			}else{
				res.render('settings.ejs', {
					user : req.user, // get the user out of session and pass to template
					messageError: 'Ha ocurrido un error actualizando sus datos.'
				});
			}
			
		});
		
	});
	
	
	// =====================================
	// LOGOUT ==============================
	// =====================================
	app.get('/logout', function(req, res) {
		req.logout();
		res.redirect('/');
	});
	
	// Handle 404
	app.use(function(req, res) {
		res.status(400);
		res.sendfile('public/grumpy.jpg', {});
	});
	
	// Handle 500
	app.use(function(req, res) {
		res.status(500);
		res.sendfile('public/500.gif', {});
	});
};

// route middleware to make sure
function isLoggedIn(req, res, next) {

	// if user is authenticated in the session, carry on
	if (req.isAuthenticated())
		return next();

	// if they aren't redirect them to the home page
	res.redirect('/');
}
