Para ejecutar fidel-admin-ui seguir estos pasos:

1) Instalar nodejs: http://nodejs.org/

2) Desde línea de comando posicionarse en el directorio del proyecto que contiene el archivo package.json e instalar las dependencias necesarias ejecutando: 
npm install

3) Instalar módulo Supervisor para evitar detener el servidor ante cambios y restart automático ante fallas:
npm install supervisor -g

4) Terminada la instalación de dependencias, iniciar el servidor ejecutando:
supervisor server.js

5) El servidor escuchará en el puerto 8080 (para cambiarlo modificar el archivo server.js y volver a iniciarlo)
 >> var port    = 	process.env.PORT || 8080;