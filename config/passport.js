// config/passport.js

// load all the things we need
var LocalStrategy   = require('passport-local').Strategy;
var dao				= require('../app/dao');
var services		= require('../app/services');

// expose this function to our app using module.exports
module.exports = function(passport) {

	// =========================================================================
    // passport session setup ==================================================
    // =========================================================================
    // required for persistent login sessions
    // passport needs ability to serialize and unserialize users out of session

    // used to serialize the user for the session
    passport.serializeUser(function(token, done) {
		//console.log('serializando:' + JSON.stringify(token));
        done(null, token);
    });

    // used to deserialize the user
    passport.deserializeUser(function(token, done) {
        
    	console.log('deserializando');
    	
    	var arr = token.split(':');
		var user = arr[0];
		
		dao.findUsuario(user, function(data){
			console.log('se recupero ' + JSON.stringify(data));
			if (data!=undefined ){
				data[0].token = token;
				return done(null, data[0]);
			}else{
				return done(null, false, { message: 'Unauthorized' });
			}
		});
		
    });

 	// =========================================================================
    // LOCAL LOGIN =============================================================
    // =========================================================================
    // we are using named strategies since we have one for login and one for signup
    // by default, if there was no name, it would just be called 'local'

    passport.use('local-login', new LocalStrategy({
		// by default, local strategy uses username and password, we will override with email
        usernameField : 'username',
        passwordField : 'password',
        passReqToCallback : true // allows us to pass back the entire request to the callback
    },
    function(req, email, password, done) { // callback with email and password from our form

    	services.authenticate(email, password, function(data){
    		
    		if(data){
    			return done(null, data);
    		}else{
    			return done(null, false, req.flash('loginMessage', 'Usuario/Password incorrectos.'));
    		}
    	});
	
    }));

};
