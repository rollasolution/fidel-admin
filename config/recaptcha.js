//Re-captcha configuration
var Recaptcha = require('re-captcha');
var PUBLIC_KEY  = '6Lcq2fYSAAAAAPktgXnHkNNLv4ZgUM_p8ZO25jdy';
var PRIVATE_KEY = '6Lcq2fYSAAAAAAcqsq77CyEj6RennBbzCBmNhVcN';
var recaptcha = new Recaptcha(PUBLIC_KEY, PRIVATE_KEY);

exports.renderChallenge = function(callback){
	console.log("Re-captcha: getting challenge...");
	callback(recaptcha.toHTML());
};
	
exports.verifyChallenge = function(req, callback){
	console.log("Re-captcha: verifying challenge...");
	
	var data = {
		    remoteip:  req.connection.remoteAddress,
		    challenge: req.body.recaptcha_challenge_field,
		    response:  req.body.recaptcha_response_field
	};

	recaptcha.verify(data, function(err) {
		if (err){
			callback(recaptcha.toHTML(err));
		}else{
			callback('0k');
		}
	});
	
};